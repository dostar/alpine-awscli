This image has the AWS CLI installed on an Alpine Linux base.

The Alpine Linux version and the AWS CLI version make up the tag of the image. In the following example, the `aws` CLI version `1.16.79` is started inside a container based on `3.8` Alpine Linux.
```
docker run --rm dostar/alpine-awscli:3.8-1.16.79 aws --version
```

You can start configuring your AWS installation with:
```
docker run --rm -t $(if tty &>/dev/null; then echo "-i"; fi) \
    -v${HOME}:${HOME} -eHOME -eUSER -u=$(id -u) \
    dostar/alpine-awscli:3.8-1.16.79 aws configure
```

Following this, you can review the configuration persisted on your host with `cat ${HOME}/.aws/config`.

https://hub.docker.com/r/dostar/alpine-awscli