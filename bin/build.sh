#!/bin/sh

PROJECT_ROOT="$( cd "$(dirname "$0")"/.. ; pwd -P )"

echo "PROJECT_ROOT=${PROJECT_ROOT}"

cd ${PROJECT_ROOT}/docker-context

ALPINE_BASE_TAG=3.10.4-alpine3.15

(set -x; docker build -t alpine-awscli:${ALPINE_BASE_TAG} \
    --build-arg ALPINE_BASE_TAG=${ALPINE_BASE_TAG} \
    .)

AWS_VERSION=$(2>&1 docker run --rm alpine-awscli:${ALPINE_BASE_TAG} aws --version | cut -f1 -d' ' | cut -f2 -d/)

: ${AWS_VERSION?aws version could not be determined}

echo "Created image with AWS_VERSION=${AWS_VERSION}"

(set -x; docker tag alpine-awscli:${ALPINE_BASE_TAG} alpine-awscli:${ALPINE_BASE_TAG}-${AWS_VERSION})

TAGS=
TAGS="${TAGS} dostar/alpine-awscli:${ALPINE_BASE_TAG}-${AWS_VERSION}"
TAGS="${TAGS} dostar/alpine-awscli:${ALPINE_BASE_TAG}"
TAGS="${TAGS} dostar/alpine-awscli"

for TAG in ${TAGS}; do
    (set -x; docker tag alpine-awscli:${ALPINE_BASE_TAG} ${TAG})
    (set -x; docker push ${TAG})
done
